package snakeyaml

import org.snakeyaml.engine.v2.api.DumpSettings
import org.snakeyaml.engine.v2.api.StreamDataWriter
import org.snakeyaml.engine.v2.common.ScalarStyle
import org.snakeyaml.engine.v2.emitter.Emitter
import org.snakeyaml.engine.v2.events.DocumentStartEvent
import org.snakeyaml.engine.v2.events.ImplicitTuple
import org.snakeyaml.engine.v2.events.ScalarEvent
import org.snakeyaml.engine.v2.events.StreamStartEvent
import java.io.StringWriter
import java.util.*

fun main(args: Array<String>) {
    println("Empty string")
    println(encodeString(""))
    println()

    println("Non-empty string")
    println(encodeString("Hello world"))
    println()
}

fun encodeString(value: String): String {
    val writer = object : StringWriter(), StreamDataWriter {
        override fun flush() { }
    }

    val settings = DumpSettings.builder().setExplicitStart(false).build()
    val emitter = Emitter(settings, writer)

    emitter.emit(StreamStartEvent())
    emitter.emit(DocumentStartEvent(false, Optional.empty(), emptyMap()))
    emitter.emit(ScalarEvent(Optional.empty(), Optional.empty(), ImplicitTuple(true, true), value, ScalarStyle.DOUBLE_QUOTED))

    return writer.toString()
}