plugins {
    id("org.jetbrains.kotlin.jvm") version "1.3.72"
    application
}

repositories {
    jcenter()
}

dependencies {
    implementation(platform("org.jetbrains.kotlin:kotlin-bom"))
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    implementation(group = "org.snakeyaml", name = "snakeyaml-engine", version = "2.1")
}

application {
    mainClassName = "snakeyaml.AppKt"
}
